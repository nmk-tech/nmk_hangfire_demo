﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using hangfire_demo.Models;
using hangfire_demo.Business;
using Hangfire;

namespace hangfire_demo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        IHangfireBusiness _hangfireBus;
        IDataBusiness _dataBus;

        public HomeController(
            ILogger<HomeController> logger, 
            IHangfireBusiness hangfireBus,
            IDataBusiness dataBus)
        {
            _logger = logger;
            _hangfireBus = hangfireBus;
            _dataBus = dataBus;
        }

        public IActionResult Index()
        {
            _hangfireBus.AddRecurringJob("job1", () => _dataBus.DoTask(),"");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
