
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hangfire;

namespace hangfire_demo.Business
{
    public class HangfireBusiness : IHangfireBusiness
    {
        public void DoJobs (params Expression<Func<Task>>[] jobs)
        {
            foreach (var job in jobs) 
            {
                var compiledJob = job.Compile();
                compiledJob().Wait();
            }
        }

        // public string AddJob(Expression<Action> job) => BackgroundJob.Enqueue(job);

        public string AddJob(Expression<Func<Task>> job) => BackgroundJob.Enqueue(job);

        // public string AddDelayedJob(Expression<Func<Task>> job, TimeSpan time) => BackgroundJob.Schedule(job, time);

        public void AddRecurringJob(string id, Expression<Func<Task>> job, string cron)
            => RecurringJob.AddOrUpdate(id, job, string.IsNullOrWhiteSpace(cron) ? Cron.Never() : cron);
        
        public void AddRecurringJobs(string id, string cron, params Expression<Func<Task>>[] jobs)
        {
            RecurringJob.AddOrUpdate(
                id, 
                () => DoJobs(jobs),
                string.IsNullOrWhiteSpace(cron) ? Cron.Never() : cron
            );
        }

        public void AddContinuousJobs(string id, params Expression<Func<Task>>[] jobs)
        {
            try
            {
                string jobId = id;
                for (var i = 1; i < jobs.Count(); i++)
                {
                    jobId = BackgroundJob.ContinueJobWith(jobId, jobs.ElementAt(i));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error has occurred! \n{Environment.NewLine} \n{ex.StackTrace}");
            }
        }

        public void TriggerJob(string id) => RecurringJob.Trigger(id);
    }
}