using System.Threading.Tasks;

namespace hangfire_demo.Business
{
    public class DataBusiness : IDataBusiness
    {
        public async Task DoTask()
        {
            await Utils.ProcessInTimeAsync(7000);
            await Utils.ProcessInTimeAsync(5000);
            await Utils.ProcessInTimeAsync(3000);
        }
    }
}