using System;
using System.Threading.Tasks;

public static class Utils 
{
    
    public static void ProcessInTime (int time) 
    {
        Console.WriteLine($"Task's processed for {time}ms...");
        try 
        {
            Task.Delay(time).Wait();
            Console.WriteLine($"...after {time}ms, it has been done.");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            Console.WriteLine($"😌😌😌😌😌😌😌😌😌😌😌😌");
        }
    }

    public static async Task ProcessInTimeAsync (int time) 
    {
        Console.WriteLine($"Task's processed for {time}ms...");
        await Task.Delay(time);
        Console.WriteLine($"...after {time}ms, it has been done.");
        Console.WriteLine($"😌😌😌😌😌😌😌😌😌😌😌😌");
    }

}