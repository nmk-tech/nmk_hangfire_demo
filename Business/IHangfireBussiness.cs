
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace hangfire_demo.Business
{
    public interface IHangfireBusiness
    {
        const string defaultId = "default";

        string AddJob(Expression<Func<Task>> job);
        
        void AddRecurringJob(string id, Expression<Func<Task>> job, string cron = "");
        void AddRecurringJobs(string id, string cron, params Expression<Func<Task>>[] jobs);

        void AddContinuousJobs(string id, params Expression<Func<Task>>[] jobs);

        void TriggerJob(string id);
    }
}