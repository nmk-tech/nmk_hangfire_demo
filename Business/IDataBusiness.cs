using System.Threading.Tasks;

namespace hangfire_demo.Business
{
    public interface IDataBusiness
    {
        Task DoTask();
    }
}